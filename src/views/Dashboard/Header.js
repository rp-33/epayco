import React from 'react';
import logo from '../../assets/Group-1.png';

const Header = ()=>{
	return(
		<div className="header">
			<div className="d-block d-md-none">
				<img 
					src = {logo}
					alt = "logo"
				/>
				<a href="#">Dashboard</a>
				<a href="#">Clientes</a>
			</div>
		</div>
	)
}

export default Header;