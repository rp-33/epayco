import React from 'react';
import imgLogo from '../../assets/logo.png';
import imgCliente from '../../assets/group.png';
import imgHouse from '../../assets/house-outline.png';

const SideBar = ()=>{
	return (
		<div className="sidebar d-none d-md-block">
			<div>
				<img 
					className="logo"
					src = {imgLogo}
				/>
			</div>
			<p>Ricardo Perez</p>
			<ol>
				<li>
					<img 
					style={{width:18,height:18,marginRight:10}}
					src = {imgHouse}
					/>
					<span>Dashboard</span>
				</li>
				<li>
					<img 
					style={{width:18,height:18,marginRight:10}}
					src = {imgCliente}
					/>
					<span>Clientes</span>
				</li>
			</ol>
		</div>
	)
}

export default SideBar;