import React,{useState,useEffect} from 'react';
import { 
	Col,
	Row,
	Form,
	Button
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {getCountry} from '../../api/user';

const Content = ({handleModal})=>{
	const [name,setName] = useState('');
	const [lastName,setLastname] = useState('');
	const [code,setCode] = useState('');
	const [countries,setCountries] = useState([]);
	const [country,setCountry] = useState('')

	const handleApiCountry = async()=>{
		try
		{
			let response = await getCountry();
			setCountries(response.slice(0,19));
			setCountry(response[0])
		}
		catch(err)
		{
			alert('error al cargar los paises')
		}
	}

	useEffect(()=>{
		handleApiCountry();
	},[]);


	const handleChange = (event)=>{
		let {name,value} = event.target
		switch (name) {
      		case 'name':
        		return setName(value)
        	case 'lastName':
        		return setLastname(value)
      		default:
        		return setCode(value)
    	}
	}

	const handleSelect = (event)=>{
		setCountry(event.target.value)
	}

	
	const handleErrorForm = (name,lastName,code)=>{
		if(name === '' || lastName === '' || code === '' || country === '') return true
		return false
	}

	const handleReset = ()=>{
		setName('');
		setLastname('');
		setCode('');
	}

	const handleSubmit = (event)=>{
		event.preventDefault();
		let validate = handleErrorForm(name,lastName,code)
		handleModal(validate);
		if(!validate)
		{
			handleReset();
		}

	}

	return(
		<div className="content-form">
			<h4>Informacion del formulario</h4>
			<Form onSubmit = {handleSubmit}>
  				<Row>
    				<Col xs={12} md={6}>
    					<Form.Group controlId="formBasicEmail">
    						<Form.Label>Nombres</Form.Label>
    						<Form.Control 
    							type="text"
    							value = {name}
    							name = "name"
    							onChange = {handleChange}
    						/>
  						</Form.Group>
    				</Col>
    				<Col xs={12} md={6}>
    					<Form.Group controlId="formBasicEmail">
    						<Form.Label>Apellidos</Form.Label>
    						<Form.Control 
    							type="text"
    							value = {lastName}
    							name="lastName"
    							onChange = {handleChange}
    						/>
  						</Form.Group>
    				</Col>
  				</Row>	
  				<Row>
    				<Col xs={12} md={6}>
    					<Form.Group controlId="exampleForm.ControlSelect1">
    						<Form.Label>Example select</Form.Label>
    						<Form.Control as="select" onChange = {handleSelect}>
    							{
    								countries.map((item,i)=>{
    									return(<option key = {i}>{item.name}</option>)
    								})
    							}
    						</Form.Control>
  						</Form.Group>
    				</Col>
    				<Col xs={12} md={6}>
    					<Form.Group controlId="formBasicEmail">
    						<Form.Label>Numero de documento</Form.Label>
    						<Form.Control 
    							type="number"
    							value = {code}
    							name = "code"
    							onChange = {handleChange}
    						/>
  						</Form.Group>
    				</Col>
  				</Row>	 				
  					<Button variant="primary" type="submit">Enviar</Button>
					<Button type="button" style={{marginLeft:20}} variant="outline-danger" onClick = {handleReset}>Cancelar</Button>
			</Form>
		</div>
	)
}

Content.propTypes = {
	handleModal :  PropTypes.func.isRequired
}

export default Content;