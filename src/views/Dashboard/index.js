import React,{useState} from 'react';
import Header from './Header';
import Sidebar from './Sidebar';
import Content from './Content';
import Footer from './Footer';
import ModalMessage from '../../components/ModalMessage';
import './style.css';

const Dashboard = ()=>{
	const [show,setShow] = useState(false);
	const [error,setError] = useState(false)

	const handleClose = ()=>{
		setShow(!show)
	}

	const handleModal = (error)=>{
		setError(error);
		setShow(!show)
	}

	return(
		<div className="dashboard">
			<Sidebar />
			<section className="content-center">
				<Header />
				<Content 
					handleModal = {handleModal}
				/>
				<Footer />
			</section>	
			<ModalMessage 
				show = {show}
				handleClose = {handleClose}
				error = {error}
			/>
		</div>
	)
}

export default Dashboard;