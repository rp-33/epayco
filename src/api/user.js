import axios from 'axios';


export const getCountry = ()=>{
	return axios.get('https://restcountries.eu/rest/v2/all')
	.then((response)=>{return response.data})
	.catch((err)=>{return err.response.data})
}