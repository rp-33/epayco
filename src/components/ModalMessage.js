import React from 'react';
import { 
	Modal,
	Button
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import imgError from '../assets/error.png'
import imgExito from '../assets/exito.png'

const ModalMessage = ({show,handleClose,error})=>{
	return(
	<Modal show={show} onHide={handleClose}>
        <Modal.Body>
        {error
        ?
        	<div style={{margin:'auto',textAlign:'center'}}>
        		<img 
        			src = {imgError}
        			alt = "error"
        			style={{width:50,height:50,marginBottom:10}}
        		/>
        		<h6 >formulario Contiene errores.</h6>
        	</div>
        :
        	<div style={{margin:'auto',textAlign:'center'}}>
        		<img 
        			src = {imgExito}
        			alt = "error"
        			style={{width:50,height:50,marginBottom:10}}
        		/>
        		<h6>formulario Enviado con exito.</h6>
        	</div>
        }
        </Modal.Body>
        <Modal.Footer>
        	<Modal.Body>
        		{error
        		?
        			<Button variant="primary" block onClick={handleClose}>
        		    	Regresar
        		  	</Button>
        		:
        			<Button variant="primary" block onClick={handleClose}>
        		    	Finalizar
        		  </Button>
        		}
           </Modal.Body>
        </Modal.Footer>
     </Modal>
	)
}

ModalMessage.propTypes = {
	show : PropTypes.bool.isRequired,
	handleClose : PropTypes.func.isRequired
}

export default ModalMessage;